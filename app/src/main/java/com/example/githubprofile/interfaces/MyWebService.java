package com.example.githubprofile.interfaces;

import com.example.githubprofile.responseModel.RepoModel;
import com.example.githubprofile.responseModel.UserModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface MyWebService {
    String BASE_URL="https://api.github.com/";


    Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

    @GET("users/{username}")
    Call<UserModel> getUserDetails(@Path("username") String username);
    @GET("users/{username}/repos")
    Call<List<RepoModel>> getUserRepos(@Path("username") String username);

}
