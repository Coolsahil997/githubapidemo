package com.example.githubprofile.repository;

import android.content.Context;
import android.util.Log;

import com.example.githubprofile.interfaces.MyWebService;
import com.example.githubprofile.observable.MyObservale;
import com.example.githubprofile.responseModel.RepoModel;
import com.example.githubprofile.responseModel.UserModel;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppRepository {
    private static AppRepository ourInstance;
    private Executor mExecutor = Executors.newSingleThreadExecutor();
    UserModel mUser;
    List<RepoModel> repoModels;
    MutableLiveData<List<RepoModel>> repoModelMutableLiveData = new MutableLiveData<>();
    public static AppRepository getInstance(Context context) {
        return ourInstance = new AppRepository();
    }
    public MutableLiveData<List<RepoModel>> getUserRepo(String userName) {
        MyWebService webService = MyWebService.retrofit.create(MyWebService.class);

        Call<List<RepoModel>> call = webService.getUserRepos(userName);
        call.enqueue(new Callback<List<RepoModel>>() {
            @Override
            public void onResponse(Call<List<RepoModel>> call, final Response<List<RepoModel>> response) {
                if (response.isSuccessful()) {
                    repoModels = response.body();
                    repoModelMutableLiveData.setValue(repoModels);
                    /*mExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            MyObservale.getInstance().setData(repoModel, 1);
                        }
                    });*/
                }
            }

            @Override
            public void onFailure(Call<List<RepoModel>> call, Throwable t) {
                Log.e("failure", t.getMessage());
                repoModelMutableLiveData.setValue(null);
                repoModels = null;
            }
        });
        return repoModelMutableLiveData;
    }

    public void getUserDetails(String userName) {
        MyWebService webService = MyWebService.retrofit.create(MyWebService.class);

        Call<UserModel> call = webService.getUserDetails(userName);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, final Response<UserModel> response) {
                if (response.isSuccessful()) {
                    mUser = response.body();
                    mExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            MyObservale.getInstance().setData(mUser, 1);
                        }
                    });
                }
                else
                {
                    MyObservale.getInstance().setData(null, 1);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                Log.e("failure", t.getMessage());
                mUser = null;
                MyObservale.getInstance().setData(null, 1);
            }
        });
    }

}
