package com.example.githubprofile.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.githubprofile.R;
import com.example.githubprofile.adapter.RepoAdapter;
import com.example.githubprofile.interfaces.IRecyclerViewClickListener;
import com.example.githubprofile.observable.MyObservale;
import com.example.githubprofile.responseModel.RepoModel;
import com.example.githubprofile.responseModel.UserModel;
import com.example.githubprofile.viewModel.MainActivityViewmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements java.util.Observer, IRecyclerViewClickListener {
    private static final String TAG = "mainActivity";
    private EditText et_search;
    private TextView tv_name, tv_email, tv_location, tv_repo, tv;
    private RecyclerView rv_repos;
    private Button btn_search;
    private MainActivityViewmodel mainActivityViewmodel;
    private IRecyclerViewClickListener iRecyclerViewClickListener;
    private Executor mExecutor = Executors.newSingleThreadExecutor();
    private List<RepoModel> repoModels;
    private UserModel userModel;
    private RepoAdapter repoAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int visibleItemCount, totalItemCount, previousTotal, visibleThreshold = 10, firstVisibleItem;
    private boolean loading;
    private String searchText;
    private ProgressDialog dialog;
    private LinearLayout ll1;
    private int count = 1;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        img = findViewById(R.id.img);
        tv = findViewById(R.id.tv);
        ll1 = findViewById(R.id.ll1);
        tv_email = findViewById(R.id.tv_email);
        tv_location = findViewById(R.id.tv_location);
        tv_name = findViewById(R.id.tv_name);
        tv_repo = findViewById(R.id.tv_repo);
        repoModels = new ArrayList<>();
        iRecyclerViewClickListener = this;
        et_search = findViewById(R.id.et_search);
        rv_repos = findViewById(R.id.rv_repos);
        btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_search.getText().length() > 0) {
                    searchText = et_search.getText().toString();
                    getUser(searchText);
                } else {
                    Toast.makeText(MainActivity.this, "Please enter UserName", Toast.LENGTH_LONG).show();
                }
            }
        });
        mainActivityViewmodel = new ViewModelProvider(this).get(MainActivityViewmodel.class);
        linearLayoutManager = new LinearLayoutManager(this);
        rv_repos.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_repos.getContext(),
                linearLayoutManager.getOrientation());
        rv_repos.addItemDecoration(dividerItemDecoration);
        repoAdapter = new RepoAdapter(MainActivity.this, repoModels, iRecyclerViewClickListener);
        rv_repos.setAdapter(repoAdapter);
     /*   rv_photos.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {

                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if (totalItemCount > previousTotal) {
//                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!loading && (totalItemCount - visibleItemCount) <= firstVisibleItem + visibleThreshold) {
                        getIssues(et_search.getText().toString());
                    }
                }
            }
        });*/
    }

    private void getUser(final String search_text) {
        loading = true;
        showProgress();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mainActivityViewmodel.getUserDetails(search_text);
            }
        });
    }

    private void showData(UserModel userModel) {
        if (userModel != null) {
            Uri uri = null;
            loading = false;
            ll1.setVisibility(View.VISIBLE);
            tv_repo.setText(String.valueOf(userModel.getPublicRepos()));
            tv_name.setText(String.valueOf(userModel.getName()));
            tv_location.setText(String.valueOf(userModel.getLocation()));
            tv_email.setText(String.valueOf(userModel.getEmail()));
            if (userModel.getAvatarUrl() != null) {
                uri = Uri.parse(userModel.getAvatarUrl());
            }
            if (uri != null) {
                Glide.with(this)
                        .load(uri)
                        .placeholder(R.drawable.ic_launcher_background)
                        .transform(new RoundedCorners(20))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(img);
            }
            mainActivityViewmodel.getNewsRepository(searchText).observe(this, new Observer<List<RepoModel>>() {
                @Override
                public void onChanged(List<RepoModel> repoModels) {
                    if (repoModels != null) {
                        if (repoModels.size() > 0) {
                            tv.setVisibility(View.VISIBLE);
                            rv_repos.setVisibility(View.VISIBLE);
                            repoAdapter = new RepoAdapter(MainActivity.this, repoModels, iRecyclerViewClickListener);
                            rv_repos.setAdapter(repoAdapter);
                            repoAdapter.notifyDataSetChanged();
                        } else {
                            rv_repos.setVisibility(View.GONE);
                            tv.setVisibility(View.GONE);
                        }
                    }
                }
            });

        } else {
            ll1.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, "No User exist related to search", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyObservale.getInstance().addObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyObservale.getInstance().deleteObserver(this);
    }

    @Override
    public void onRecyclerClick(View view, int position) {

    }

    @Override
    public void update(Observable observable, final Object o) {
        try {
            dismissProgress();
            userModel = (UserModel) o;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showData(userModel);
                }
            });

        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    private void showProgress() {
        if (dialog == null) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Loading, please wait.");
        }
        //   dialog.show();
    }

    private void dismissProgress() {

       /* if (dialog.isShowing()) {
            dialog.dismiss();
        }*/
    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            if (count == 0) {
                super.onBackPressed();
            } else {
                count--;
                Toast.makeText(MainActivity.this, "Press again to exit", Toast.LENGTH_LONG).show();
            }

            Log.i(TAG, "This is last activity in the stack");
        }
    }
}
