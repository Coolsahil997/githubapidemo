package com.example.githubprofile.adapter;

import android.app.Activity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.githubprofile.R;
import com.example.githubprofile.interfaces.IRecyclerViewClickListener;
import com.example.githubprofile.responseModel.RepoModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {
    private Activity context;
    private IRecyclerViewClickListener onRecyclerItemClickListener;
    private List<RepoModel> repoModels = new ArrayList<>();

    public RepoAdapter(Activity context, List<RepoModel> repoModels, IRecyclerViewClickListener onRecyclerItemClickListener) {
        this.repoModels.clear();
        this.repoModels.addAll(repoModels);
        this.context = context;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    static class RepoViewHolder extends RecyclerView.ViewHolder {
        TextView tv_repo_name, tv_repo_desc, tv_default_branch;

        RepoViewHolder(View itemView) {
            super(itemView);
            tv_repo_name = itemView.findViewById(R.id.tv_repo_name);
            tv_repo_desc = itemView.findViewById(R.id.tv_repo_desc);
            tv_default_branch = itemView.findViewById(R.id.tv_default_branch);
        }
    }


    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cell, parent, false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        bindItem(holder);
    }

    private void bindItem(final RepoViewHolder holder) {
        final RepoModel repoModel = repoModels.get(holder.getAdapterPosition());
        holder.tv_default_branch.setText(repoModel.getDefault_branch());
        holder.tv_repo_desc.setText(repoModel.getDescription());
        holder.tv_repo_name.setClickable(true);
        holder.tv_repo_name.setMovementMethod(LinkMovementMethod.getInstance());
        String name="<a href='"+repoModel.getHtml_url()+"'>"+repoModel.getName()+"</a>";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.tv_repo_name.setText(Html.fromHtml(name, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.tv_repo_name.setText(Html.fromHtml(name));
        }
    }

    @Override
    public int getItemCount() {
        Log.e("related-size", repoModels.size() + "");
        return repoModels.size();
    }
}

