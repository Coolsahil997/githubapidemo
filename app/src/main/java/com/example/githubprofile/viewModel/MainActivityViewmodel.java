package com.example.githubprofile.viewModel;

import android.app.Application;


import com.example.githubprofile.repository.AppRepository;
import com.example.githubprofile.responseModel.RepoModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainActivityViewmodel
        extends AndroidViewModel {

    MutableLiveData<List<RepoModel>> repoModelMutableLiveData = new MutableLiveData<>();

    private AppRepository mAppRepository;

    public MainActivityViewmodel(@NonNull Application application) {
        super(application);
        mAppRepository = AppRepository.getInstance(application.getApplicationContext());

    }

    public void getUserDetails(String userName) {
         mAppRepository.getUserDetails(userName);
    }
    public LiveData<List<RepoModel>> getNewsRepository(String userName) {
        return mAppRepository.getUserRepo(userName);
    }
}
